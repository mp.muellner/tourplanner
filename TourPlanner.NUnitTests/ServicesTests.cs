using Microsoft.EntityFrameworkCore;
using Moq;
using System.Collections.ObjectModel;
using System.Windows;
using TourPlanner.Models;
using TourPlanner.Services;

namespace TourPlanner.NUnitTests

{

    [TestFixture]
    //Apartment is needed for UI testing.
    [Apartment(ApartmentState.STA)]
    public class WindowAlignmentTests
    {
        [Test]
        public void GetLeftPosition_ReturnsCorrectLeftPosition()
        {
            // Arrange
            var mainWindow = new Window { Left = 100, Width = 200 };
            var window = new Window();

            // Simulate setting the main window in the current application
            var application = Application.Current ?? new Application();
            application.MainWindow = mainWindow;

            // Act
            var result = WindowAlignment.GetLeftPosition(window);

            // Assert
            var expected = mainWindow.Left - window.Width - 10;
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void GetTopPosition_ReturnsCorrectTopPosition()
        {
            // Arrange
            var mainWindow = new Window { Top = 50 };

            // Simulate setting the main window in the current application
            var application = Application.Current ?? new Application();
            application.MainWindow = mainWindow;

            // Act
            var result = WindowAlignment.GetTopPosition();

            // Assert
            var expected = mainWindow.Top;
            Assert.That(result, Is.EqualTo(expected));
        }
    }

    //[TestFixture]
    //public class TourHandlerTests
    //{
    //    [Test]
    //    public void GetAllTours_ReturnsAllTours()
    //    {
    //        // Arrange
    //        var mockTourPlannerContext = new Mock<TourPlannerContext>();
    //        // Act

    //        // Assert
    //    }

    //}


}