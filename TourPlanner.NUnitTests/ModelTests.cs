﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Moq;
using System.Collections.ObjectModel;
using System.Windows;
using TourPlanner.Models;
using TourPlanner.Services;
using System.Reflection;
using TourPlanner.Models.Converter;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using TourPlanner.Models.Enums;
using System.Configuration;
using TourPlanner.Models.Messages;

namespace TourPlanner.NUnitTests
{
    public class ModelTests
    {
        [TestFixture]
        public class ConverterTests
        {
            [Test]
            public void IsUtc_WithNullProperty_ShouldReturnTrue()
            {
                // Arrange
                IMutableProperty property = null;

                // Act
                var isUtc = property.IsUtc();

                // Assert
                Assert.IsTrue(isUtc);
            }

            
        }

        [TestFixture]
        public class MapQuestInfosTests
        {
            [Test]
            public void Constructor_WithValidArguments_InitializesProperties()
            {
                // Arrange
                double distance = 10.5;
                string estimatedTime = "2 hours";
                string routePath = "../imagePath/image";

                // Act
                MapQuestInfos mapQuestInfos = new MapQuestInfos(distance, estimatedTime, routePath);

                // Assert
                Assert.That(mapQuestInfos.Distance, Is.EqualTo(distance));
                Assert.That(mapQuestInfos.EstimatedTime, Is.EqualTo(estimatedTime));
                Assert.That(mapQuestInfos.RoutePath, Is.EqualTo(routePath));
            }

            [Test]
            public void Distance_Property_SetAndGetCorrectly()
            {
                // Arrange
                double distance = 15.2;
                MapQuestInfos mapQuestInfos = new MapQuestInfos(10.5, "2 hours", "../imagePath/image");

                // Act
                mapQuestInfos.Distance = distance;

                // Assert
                Assert.That(mapQuestInfos.Distance, Is.EqualTo(distance));
            }

            [Test]
            public void EstimatedTime_Property_SetAndGetCorrectly()
            {
                // Arrange
                string estimatedTime = "3 hours";
                MapQuestInfos mapQuestInfos = new MapQuestInfos(10.5, "2 hours", "../imagePath/image");

                // Act
                mapQuestInfos.EstimatedTime = estimatedTime;

                // Assert
                Assert.That(mapQuestInfos.EstimatedTime, Is.EqualTo(estimatedTime));
            }

            [Test]
            public void RoutePath_Property_SetAndGetCorrectly()
            {
                // Arrange
                string routePath = "../imagePath/image";
                MapQuestInfos mapQuestInfos = new MapQuestInfos(10.5, "2 hours", "../imagePath/image");

                // Act
                mapQuestInfos.RoutePath = routePath;

                // Assert
                Assert.That(mapQuestInfos.RoutePath, Is.EqualTo(routePath));
            }
        }

        [TestFixture]
        public class TourTests
        {
            //[Test]
            //public void Popularity_ReturnsCorrectPopularity()
            //{
            //    // Arrange
            //    var tour = new Tour();
            //    tour.Id = 1;
            //    tour.TourLogs = new List<TourLog>
            //{
            //    new TourLog(),
            //    new TourLog(),
            //    new TourLog()
            //};

            //    // Act
            //    var popularity = tour.Popularity;

            //    // Assert
            //    Assert.AreEqual(Popularity.Niche, popularity);
            //}

            //[Test]
            //public void AverageDifficulty_ReturnsCorrectAverageDifficulty()
            //{
            //    // Arrange
            //    var configPath = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            //    var tour = new Tour();
            //    tour.Id = 1;
            //    tour.TourLogs = new List<TourLog>
            //{
            //    new TourLog { Difficulty = Difficulty.Easy },
            //    new TourLog { Difficulty = Difficulty.Moderate },
            //    new TourLog { Difficulty = Difficulty.Challenging }
            //};

            //    // Act
            //    var averageDifficulty = tour.AverageDifficulty;

            //    // Assert
            //    Assert.AreEqual(Difficulty.Moderate, averageDifficulty);
            //}

            //[Test]
            //public void AverageTime_ReturnsCorrectAverageTime()
            //{
            //    // Arrange
            //    var tour = new Tour();
            //    tour.Id = 1;
            //    tour.TourLogs = new List<TourLog>
            //{
            //    new TourLog { Hours = 2 },
            //    new TourLog { Hours = 3 },
            //    new TourLog { Hours = 4 }
            //};

            //    // Act
            //    var averageTime = tour.AverageTime;

            //    // Assert
            //    Assert.AreEqual("3h", averageTime);
            //}

            //[Test]
            //public void AverageRating_ReturnsCorrectAverageRating()
            //{
            //    // Arrange
            //    var tour = new Tour();
            //    tour.Id = 1;
            //    tour.TourLogs = new List<TourLog>
            //{
            //    new TourLog { Rating = Rating.Good },
            //    new TourLog { Rating = Rating.Excellent },
            //    new TourLog { Rating = Rating.Poor }
            //};

            //    // Act
            //    var averageRating = tour.AverageRating;

            //    // Assert
            //    Assert.AreEqual(Rating.Good, averageRating);
            //}

            //[Test]
            //public void ChildFriendliness_ReturnsCorrectChildFriendliness()
            //{
            //    // Arrange
            //    var tour = new Tour();
            //    tour.TourDistance = 5;
            //    tour.Id = 1;
            //    tour.TourLogs = new List<TourLog>
            //    {
            //        new TourLog { Difficulty = Difficulty.Easy, Hours = 1 },
            //        new TourLog { Difficulty = Difficulty.Moderate, Hours = 2 },
            //        new TourLog { Difficulty = Difficulty.Challenging, Hours = 3 }
            //    };

            //    // Act
            //    var childFriendliness = tour.ChildFriendliness;

            //    // Assert
            //    Assert.AreEqual(ChildFriendly.VeryChildFriendly, childFriendliness);
            //}

            [Test]
            public void GetDistance_ReturnsFormattedDistance()
            {
                // Arrange
                var tour = new Tour();
                tour.TourDistance = 15.6789;

                // Act
                var distance = tour.GetDistance;

                // Assert
                Assert.AreEqual("15,68 km", distance);
            }
        }

        [TestFixture]
        public class ChildFriendlyTests
        {
            [Test]
            public void ChildFriendly_Values_Exist()
            {
                // Arrange

                // Act

                // Assert
                Assert.IsTrue(ChildFriendly.VeryChildFriendly.Equals((ChildFriendly)0));
                Assert.IsTrue(ChildFriendly.ChildFriendly.Equals((ChildFriendly)1));
                Assert.IsTrue(ChildFriendly.MostlyChildFriendly.Equals((ChildFriendly)2));
                Assert.IsTrue(ChildFriendly.PartiallyChildFriendly.Equals((ChildFriendly)3));
                Assert.IsTrue(ChildFriendly.NotChildFriendly.Equals((ChildFriendly)4));
            }
        }

        [TestFixture]
        public class DifficultyTests
        {
            [Test]
            public void Difficulty_Enum_Should_Contain_Easy()
            {
                // Arrange

                // Act

                // Assert
                Assert.IsTrue(Difficulty.Easy.ToString().Equals("Easy"));
            }

            [Test]
            public void Difficulty_Enum_Should_Contain_Moderate()
            {
                // Arrange

                // Act

                // Assert
                Assert.IsTrue(Difficulty.Moderate.ToString().Equals("Moderate"));
            }

            [Test]
            public void Difficulty_Enum_Should_Contain_Challenging()
            {
                // Arrange

                // Act

                // Assert
                Assert.IsTrue(Difficulty.Challenging.ToString().Equals("Challenging"));
            }

            [Test]
            public void Difficulty_Enum_Should_Contain_Advanced()
            {
                // Arrange

                // Act

                // Assert
                Assert.IsTrue(Difficulty.Advanced.ToString().Equals("Advanced"));
            }

            [Test]
            public void Difficulty_Enum_Should_Contain_Expert()
            {
                // Arrange

                // Act

                // Assert
                Assert.IsTrue(Difficulty.Expert.ToString().Equals("Expert"));
            }
        }



        [TestFixture]
        public class EnumBindingSourceExtensionTests
        {

            [Test]
            public void Constructor_NullEnumType_ThrowsException()
            {
                // Arrange
                
                // Act

                // Assert
                Assert.Throws<Exception>(() => new EnumBindingSourceExtension(null));
            }

            [Test]
            public void Constructor_NonEnumType_ThrowsException()
            {
                // Arrange
                var nonEnumType = typeof(string);

                // Act, Assert
                Assert.Throws<Exception>(() => new EnumBindingSourceExtension(nonEnumType));
            }
        }

        [TestFixture]
        public class RatingTests
        {
            [Test]
            public void Enum_ContainsExpectedValues()
            {
                // Arrange

                // Act

                // Assert
                Assert.AreEqual(5, System.Enum.GetValues(typeof(Rating)).Length);

                Assert.IsTrue(System.Enum.IsDefined(typeof(Rating), Rating.Poor));
                Assert.IsTrue(System.Enum.IsDefined(typeof(Rating), Rating.Fair));
                Assert.IsTrue(System.Enum.IsDefined(typeof(Rating), Rating.Good));
                Assert.IsTrue(System.Enum.IsDefined(typeof(Rating), Rating.Excellent));
                Assert.IsTrue(System.Enum.IsDefined(typeof(Rating), Rating.Outstanding));
            }
        }

        [TestFixture]
        public class TransportTypeTests
        {
            [Test]
            public void Enum_Values_Should_Exist()
            {
                // Arrange

                // Act

                // Assert
                Assert.That(Enum.IsDefined(typeof(TransportType), TransportType.Fastest));
                Assert.That(Enum.IsDefined(typeof(TransportType), TransportType.Shortest));
                Assert.That(Enum.IsDefined(typeof(TransportType), TransportType.Pedestrian));
                Assert.That(Enum.IsDefined(typeof(TransportType), TransportType.Bicycle));
            }

            [Test]
            public void Enum_Fastest_Should_Have_Correct_Value()
            {
                // Arrange

                // Act
                var value = (int)TransportType.Fastest;

                // Assert
                Assert.That(value, Is.EqualTo(0));
            }

            [Test]
            public void Enum_Shortest_Should_Have_Correct_Value()
            {
                // Arrange

                // Act
                var value = (int)TransportType.Shortest;

                // Assert
                Assert.That(value, Is.EqualTo(1));
            }

            [Test]
            public void Enum_Pedestrian_Should_Have_Correct_Value()
            {
                // Arrange

                // Act
                var value = (int)TransportType.Pedestrian;

                // Assert
                Assert.That(value, Is.EqualTo(2));
            }

            [Test]
            public void Enum_Bicycle_Should_Have_Correct_Value()
            {
                // Arrange

                // Act
                var value = (int)TransportType.Bicycle;

                // Assert
                Assert.That(value, Is.EqualTo(3));
            }
        }

        [TestFixture]
        public class PopularityTests
        {
            [Test]
            public void Enum_Values_Should_Exist()
            {
                // Arrange

                // Act

                // Assert
                Assert.That(Enum.IsDefined(typeof(Popularity), Popularity.Niche));
                Assert.That(Enum.IsDefined(typeof(Popularity), Popularity.Moderate));
                Assert.That(Enum.IsDefined(typeof(Popularity), Popularity.Mainstream));
                Assert.That(Enum.IsDefined(typeof(Popularity), Popularity.Trendy));
                Assert.That(Enum.IsDefined(typeof(Popularity), Popularity.Iconic));
            }

            [Test]
            public void Enum_Niche_Should_Have_Correct_Value()
            {
                // Arrange

                // Act
                var value = (int)Popularity.Niche;

                // Assert
                Assert.AreEqual(0, value);
            }

            [Test]
            public void Enum_Moderate_Should_Have_Correct_Value()
            {
                // Arrange

                // Act
                var value = (int)Popularity.Moderate;

                // Assert
                Assert.AreEqual(1, value);
            }

            [Test]
            public void Enum_Mainstream_Should_Have_Correct_Value()
            {
                // Arrange

                // Act
                var value = (int)Popularity.Mainstream;

                // Assert
                Assert.AreEqual(2, value);
            }

            [Test]
            public void Enum_Trendy_Should_Have_Correct_Value()
            {
                // Arrange

                // Act
                var value = (int)Popularity.Trendy;

                // Assert
                Assert.AreEqual(3, value);
            }

            [Test]
            public void Enum_Iconic_Should_Have_Correct_Value()
            {
                // Arrange

                // Act
                var value = (int)Popularity.Iconic;

                // Assert
                Assert.AreEqual(4, value);
            }
        }

        [TestFixture]
        public class AllTourImagesUpdatedMessageTests
        {
            [Test]
            public void AllTourImagesUpdated() 
            {
                //Arrange
                var tourLogImgage = new TourLogImage();
                
                var list = new List<TourLogImage>();

                //Act
                list.Add(tourLogImgage);

                var allTourImagesUpdatedMessage = new AllTourImagesUpdatedMessage(list);

                //Assert
                Assert.IsTrue(allTourImagesUpdatedMessage.ImageList.Count > 0);
            }
        }

        [TestFixture]
        public class DbTourLogsUpdatedMessageTests
        {
            [Test]
            public void DbTourLogsUpdated()
            {
                //Arrange
                var TourLog = new TourLog();

                var list = new ObservableCollection<TourLog>();

                //Act
                list.Add(TourLog);

                var dbTourLogsUpdatedMessage = new DbTourLogsUpdatedMessage(list);

                //Assert
                Assert.IsTrue(dbTourLogsUpdatedMessage.TourLogsCollection.Count > 0);
            }
        }

        [TestFixture]
        public class DbToursUpdatedMessageTests
        {
            [Test]
            public void DbToursUpdated()
            {
                //Arrange
                var Tour = new Tour();

                var list = new ObservableCollection<Tour>();

                //Act
                list.Add(Tour);

                var dbTourLogsUpdatedMessage = new DbToursUpdatedMessage(list);

                //Assert
                Assert.IsTrue(dbTourLogsUpdatedMessage.TourCollection.Count > 0);
            }
        }
    }
}
