﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TourPlanner.Models
{
    /// <summary>
    /// Simple Class for storing MapQuestAPI Information
    /// </summary>
    public class MapQuestInfos
    {
        public double Distance;
        public string EstimatedTime;
        public string RoutePath;

        public MapQuestInfos(double distance, string estimatedTime, string routePath)
        {
            Distance = distance;
            EstimatedTime = estimatedTime;
            RoutePath = routePath;
        }
    }
}
