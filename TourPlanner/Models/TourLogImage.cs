﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommunityToolkit.Mvvm.ComponentModel;

namespace TourPlanner.Models
{
    [Table("tour_log_images")]
    public class TourLogImage : ObservableObject
    {
        [Column("id")]
        [Key]
        public int Id { get; set; }

        [Column("name")]
        [Required]
        public string Name { get; set; }

        [Column("imagePath")]
        [Required]
        public string ImagePath { get; set; }

        public int TourLogId { get; set; }
        public TourLog TourLog { get; set; }
    }
}
