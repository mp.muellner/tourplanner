﻿using CommunityToolkit.Mvvm.Messaging.Messages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TourPlanner.Views;

namespace TourPlanner.Models.Messages
{
    /// <summary>
    /// Message for ValueChanges regarding ObservableCollection of TourLogs
    /// </summary>
    public class DbTourLogsUpdatedMessage : ValueChangedMessage<ObservableCollection<TourLog>>
    {
        public ObservableCollection<TourLog> TourLogsCollection { get; set; }

        public DbTourLogsUpdatedMessage(ObservableCollection<TourLog> value) : base(value)
        {
            TourLogsCollection = value;
        }
    }
}
