﻿using CommunityToolkit.Mvvm.Messaging.Messages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TourPlanner.Models.Messages
{
    public class TourImagesUpdatedMessage : ValueChangedMessage<List<TourLogImage>>
    {
        public List<TourLogImage> ImageList { get; set; }

        public TourImagesUpdatedMessage(List<TourLogImage> value) : base(value)
        {
            ImageList = value;
        }
    }
}
