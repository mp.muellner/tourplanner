﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommunityToolkit.Mvvm.Messaging.Messages;

namespace TourPlanner.Models.Messages
{
    /// <summary>
    /// RequestMessage for requesting selected Tour
    /// </summary>
    public class SelectedTourRequestMessage : RequestMessage<Tour>
    {
    }
}
