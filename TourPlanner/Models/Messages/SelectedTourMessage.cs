﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommunityToolkit.Mvvm.Messaging.Messages;

namespace TourPlanner.Models.Messages
{
    /// <summary>
    /// Message for ValueChanges regarding ObservableCollection of Tours
    /// </summary>
    public class SelectedTourMessage : ValueChangedMessage<Tour>
    {
        public Tour Tour { get; set; }

        public SelectedTourMessage(Tour value) : base(value)
        {
            Tour = value;
        }
    }
}
