﻿using CommunityToolkit.Mvvm.Messaging.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TourPlanner.Models.Messages
{
    /// <summary>
    /// RequestMessage for requesting selected TourLog
    /// </summary>
    public class SelectedTourLogRequestMessage : RequestMessage<TourLog>
    {
    }
}
