﻿using CommunityToolkit.Mvvm.Messaging.Messages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TourPlanner.Models.Messages
{
    /// <summary>
    /// Message for ValueChanges regarding ObservableCollection of Tours
    /// </summary>
    public class DbToursUpdatedMessage : ValueChangedMessage<ObservableCollection<Tour>>
    {
        public ObservableCollection<Tour> TourCollection { get; set; }

        public DbToursUpdatedMessage(ObservableCollection<Tour> value) : base(value)
        {
            TourCollection = value;
        }
    }
}
