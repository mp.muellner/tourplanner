﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommunityToolkit.Mvvm.Messaging.Messages;

namespace TourPlanner.Models.Messages
{
    public class AllTourImagesUpdatedMessage : ValueChangedMessage<List<TourLogImage>>
    {
        public List<TourLogImage> ImageList { get; set; }

        public AllTourImagesUpdatedMessage(List<TourLogImage> value) : base(value)
        {
            ImageList = value;
        }
    }
}
