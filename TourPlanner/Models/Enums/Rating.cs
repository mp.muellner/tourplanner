﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TourPlanner.Models.Enums
{
    /// <summary>
    /// Enums for selecting/displaying Rating Levels
    /// </summary>
    public enum Rating
    {
        Poor,
        Fair,
        Good,
        Excellent,
        Outstanding
    }
}
