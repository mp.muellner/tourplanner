﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace TourPlanner.Models.Enums
{
    /// <summary>
    /// Enums for selecting/displaying ChildFriendliness Levels
    /// </summary>
    public enum ChildFriendly
    {
        [Display(Name = "Very Child-Friendly")]
        VeryChildFriendly,
        [Display(Name = "Child-Friendly")]
        ChildFriendly,
        [Display(Name = "Mostly Child-Friendly")]
        MostlyChildFriendly,
        [Display(Name = "Partially Child-Friendly")]
        PartiallyChildFriendly,
        [Display(Name = "Not Child-Friendly")]
        NotChildFriendly
    }
}
