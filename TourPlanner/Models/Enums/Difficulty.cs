﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TourPlanner.Models.Enums
{
    /// <summary>
    /// Enums for selecting/displaying Difficulty Levels
    /// </summary>
    public enum Difficulty
    {
        Easy,
        Moderate,
        Challenging,
        Advanced,
        Expert
    }
}
