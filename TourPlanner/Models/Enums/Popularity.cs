﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TourPlanner.Models.Enums
{
    /// <summary>
    /// Enums for selecting/displaying Popularity Levels
    /// </summary>
    public enum Popularity
    {
        Niche,
        Moderate,
        Mainstream,
        Trendy,
        Iconic
    }
}
