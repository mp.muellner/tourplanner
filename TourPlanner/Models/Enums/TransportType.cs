﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TourPlanner.Models.Enums
{
    /// <summary>
    /// Enums for selecting/displaying TransportType Levels
    /// </summary>
    public enum TransportType
    {
        Fastest,
        Shortest,
        Pedestrian,
        Bicycle
    }
}
