﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Configuration;
using System.Net;
using System.Xml.Linq;
using TourPlanner.Models.Converter;
using TourPlanner.Models.Enums;
using TransportType = TourPlanner.Models.Enums.TransportType;

namespace TourPlanner.Models
{
    /// <summary>
    /// TourPlanner DB-Context
    /// </summary>
    public class TourPlannerContext : DbContext
    {
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyUtcDateTimeConverter();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connString = ConfigurationManager.ConnectionStrings["TourPlannerDbConfig"].ToString();
            optionsBuilder.UseNpgsql(connString);
        }
        public DbSet<Tour> Tours { get; set; }
        public DbSet<TourLog> TourLogs { get; set; }
        public DbSet<TourLogImage> TourLogImages { get; set; }
    }
}
