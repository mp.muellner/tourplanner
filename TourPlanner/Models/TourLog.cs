﻿using CommunityToolkit.Mvvm.ComponentModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TourPlanner.Models.Enums;

namespace TourPlanner.Models
{
    /// <summary>
    /// TourLog Model: used with EntityFramework to create DB Table
    /// </summary>

    [Table("tour_logs")]
    public class TourLog : ObservableObject
    {
        [Column("id")]
        [Key]
        public int Id { get; set; }

        [Column("done_at")]
        [Required]
        public DateTime? DoneAt { get; set; }

        [Column("comment")]
        public string Comment { get; set; }

        [Column("difficulty")]
        [Required]
        public Difficulty? Difficulty { get; set; }

        [NotMapped]
        public string TotalTime => $"{Hours}h {Minutes}m";

        [Column("hours")]
        [Required]
        public int Hours { get; set; }

        [Column("minutes")]
        [Required]
        public int Minutes { get; set; }

        [Column("rating")]
        [Required]
        public Rating? Rating { get; set; }

        public int TourId { get; set; }
        public Tour Tour { get; set; }

        public ICollection<TourLogImage> TourLogImages { get; set; }

        /// <summary>
        /// Prettier Date Format
        /// </summary>
        [NotMapped]
        public string GetDate
        {
            get
            {
                if (DoneAt != null)
                {
                    return ((DateTime)DoneAt).ToString("dd/MM/yyyy");
                }

                return "-";
            }
        }
    }
}
