﻿using System;
using CommunityToolkit.Mvvm.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net;
using TourPlanner.Models.Enums;
using TourPlanner.Services;
using TransportType = TourPlanner.Models.Enums.TransportType;

namespace TourPlanner.Models
{
    /// <summary>
    /// Tour Model: used with EntityFramework to create DB Table
    /// </summary>
    
    [Table("tours")]
    public class Tour : ObservableObject
    {
        [Column("id")]
        [Key]
        public int Id { get; set; }

        [Column("name")]
        [Required]
        public string Name { get; set; }

        [Column("description")]
        public string Description { get; set; }

        [Column("from")]
        [Required]
        public string From { get; set; }

        [Column("to")]
        [Required]
        public string To { get; set; }

        [Column("transport_type")]
        public TransportType? TransportType { get; set; }

        [Column("tour_distance")]
        public double TourDistance { get; set; }

        [Column("estimated_time")]
        public string EstimatedTime { get; set; }

        [Column("route_information")]
        public string RouteInformation { get; set; }

        public ICollection<TourLog> TourLogs { get; set; }

        [NotMapped]
        public List<TourLog> TourLogList { get; set; }

        [NotMapped]
        public List<TourLogImage> TourLogImageList { get; set; }

        /// <summary>
        /// returns the computed attribute Popularity
        /// Popularity = number of logs
        /// </summary>
        [NotMapped]
        public Popularity Popularity {
            get
            {
                var tourHandler = new TourHandler();
                var logCount = tourHandler.GetLogs(this.Id).Count;
                return logCount switch
                {
                    <= 3 => Popularity.Niche,
                    <= 6 => Popularity.Moderate,
                    <= 9 => Popularity.Mainstream,
                    <= 12 => Popularity.Trendy,
                    _ => Popularity.Iconic
                };
            } }

        /// <summary>
        /// returns the computed attribute AverageDifficulty
        /// </summary>
        [NotMapped]
        public Difficulty AverageDifficulty => GetAverageDifficulty();

        /// <summary>
        /// returns the computed attribute AverageTime
        /// </summary>
        [NotMapped]
        public string AverageTime
        {
            get
            {
                int time = GetAverageTotalTime();
                return $"{time}h";
            }
        }

        /// <summary>
        /// returns the computed attribute AverageTime
        /// </summary>
        [NotMapped]
        public Rating AverageRating => GetAverageRating();

        /// <summary>
        /// returns the computed attribute ChildFriendliness
        /// ChildFriendliness = Combination of Tour Distance, AverageDifficulty and Average TotalTime
        /// </summary>
        [NotMapped]
        public ChildFriendly ChildFriendliness
        {
            get
            {
                if (IsDistanceChildFriendly() && GetAverageDifficulty() <= Difficulty.Easy &&
                    GetAverageTotalTime() <= 2)
                {
                    return ChildFriendly.VeryChildFriendly;
                }

                if (IsDistanceChildFriendly() && GetAverageDifficulty() <= Difficulty.Moderate &&
                    GetAverageTotalTime() <= 2)
                {
                    return ChildFriendly.ChildFriendly;
                }

                if (IsDistanceChildFriendly() && GetAverageDifficulty() <= Difficulty.Challenging &&
                    GetAverageTotalTime() <= 2)
                {
                    return ChildFriendly.MostlyChildFriendly;
                }

                if (IsDistanceChildFriendly() && GetAverageDifficulty() <= Difficulty.Advanced &&
                    GetAverageTotalTime() <= 2)
                {
                    return ChildFriendly.PartiallyChildFriendly;
                }

                return ChildFriendly.NotChildFriendly;
            }
        }

        /// <summary>
        /// Function for determining average Difficulty
        /// </summary>
        private Difficulty GetAverageDifficulty()
        {
            var tourHandler = new TourHandler();
            var tourLogs = tourHandler.GetLogs(this.Id);
            if (tourLogs.Count > 0)
            {
                return (Difficulty)tourLogs.Average(t => (decimal)t.Difficulty);
            }

            return Difficulty.Moderate;
        }

        /// <summary>
        /// Function for determining average TotalTime
        /// </summary>
        private int GetAverageTotalTime()
        {
            var tourHandler = new TourHandler();
            var tourLogs = tourHandler.GetLogs(this.Id);
            if (tourLogs.Count > 0)
            {
                return (int)tourLogs.Average(t => t.Hours);
            }

            return 0;
        }

        /// <summary>
        /// Function for determining average Rating
        /// </summary>
        private Rating GetAverageRating()
        {
            var tourHandler = new TourHandler();
            var tourLogs = tourHandler.GetLogs(this.Id);
            if (tourLogs.Count > 0)
            {
                return (Rating)tourLogs.Average(t => (decimal)t.Rating);
            }

            return Rating.Fair;
        }

        /// <summary>
        /// Function for determining if a Tour Distance is child-friendly
        /// </summary>
        private bool IsDistanceChildFriendly()
        {
            if (TourDistance <= 10)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Prettier Distance Format
        /// </summary>
        [NotMapped]
        public string GetDistance => $"{Math.Round(TourDistance, 2)} km";
    }
}
