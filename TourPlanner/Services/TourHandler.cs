﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Messaging;
using Microsoft.EntityFrameworkCore;
using Serilog;
using TourPlanner.Models;
using TourPlanner.Models.Messages;
using TourPlanner.ViewModels;
using TourPlanner.Views;

namespace TourPlanner.Services
{
    /// <summary>
    /// Service: CRUD operations for Tour/TourLogs
    /// </summary>
    public partial class TourHandler : ObservableRecipient
    {
        /// <summary>
        /// Gets all Tours from DB
        /// </summary>
        /// <returns>ObservableCollection of all tours</returns>
        public ObservableCollection<Tour> GetAllTours()
        {
            Log.Information("Retrieving all tours");
            var tpc = new TourPlannerContext();
            return new ObservableCollection<Tour>(tpc.Tours);
        }

        /// <summary>
        /// Gets all tour specific logs
        /// </summary>
        /// <param name="tourId">tour id of wanted logs</param>
        /// <returns>ObservableCollection of all specific tour logs</returns>
        public ObservableCollection<TourLog> GetLogs(int tourId)
        {
            Log.Information("Retrieving all tour logs from given tour");
            var tpc = new TourPlannerContext();
            return new ObservableCollection<TourLog>(tpc.TourLogs.Where(t => t.TourId == tourId));
        }

        /// <summary>
        /// Gets all tour logs
        /// </summary>
        /// <returns>ObservableCollection of all tour logs</returns>
        public ObservableCollection<TourLog> GetAllLogs()
        {
            Log.Information("Retrieving all tour logs");
            var tpc = new TourPlannerContext();
            return new ObservableCollection<TourLog>(tpc.TourLogs);
        }

        /// <summary>
        /// Gets a tour by given id
        /// </summary>
        /// <param name="tourId">Tour that should be returned</param>
        /// <returns>Tour</returns>
        public Tour GetTourById(int tourId)
        {
            Log.Information("Getting tour by id");
            var tpc = new TourPlannerContext();
            return tpc.Tours.Find(tourId);
        }

        /// <summary>
        /// Gets a tour log by given id
        /// </summary>
        /// <param name="tourId">Tour that should be returned</param>
        /// <returns>Tour</returns>
        public TourLog GetTourLogById(int tourLogId)
        {
            Log.Information("Getting tour log by id");
            var tpc = new TourPlannerContext();
            return tpc.TourLogs.Find(tourLogId);
        }

        /// <summary>
        /// Inserts a tour in DB and sends notification message to all recipients
        /// </summary>
        /// <param name="tour">Tour that should be inserted</param>
        public void CreateTour(Tour tour)
        {
            var tpc = new TourPlannerContext();
            tpc.Tours.Add(tour);
            tpc.SaveChanges();
            Messenger.Send(new DbToursUpdatedMessage(GetAllTours()));
            Log.Information("Tour created");
            if (tour.TourLogList != null)
            {
                foreach (TourLog tl in tour.TourLogList)
                {
                    CreateTourLog(tl);
                }
            }
            if (tour.TourLogImageList != null)
            {
                foreach (TourLogImage tli in tour.TourLogImageList)
                {
                    SaveTourImage(tour.Id, tli.TourLogId, tli);
                }
            }
        }

        /// <summary>
        /// Updates a tour in DB and sends notification message to all recipients
        /// </summary>
        /// <param name="tour">Tour that should be updated</param>
        public void UpdateTour(Tour tour)
        {
            using var tpc = new TourPlannerContext();
            tpc.Tours.Update(tour);
            tpc.SaveChanges();
            Messenger.Send(new DbToursUpdatedMessage(GetAllTours()));
            Log.Information("Tour updated");
        }

        /// <summary>
        /// Deletes a tour from DB and sends notification message to all recipients
        /// Additionally: Deletes route image
        /// </summary>
        /// <param name="tour">Tour that should be deleted</param>
        public void DeleteTour(Tour tour)
        {
            var allTourLogs = GetLogs(tour.Id);
            foreach (var tourLog in allTourLogs)
            {
                DeleteTourLog(tourLog);
            }

            using var tpc = new TourPlannerContext();
            Log.Information("Tour logs deleted");
            tpc.Tours.Remove(tour);
            tpc.SaveChanges();
            Messenger.Send(new DbToursUpdatedMessage(GetAllTours()));
            Messenger.Send(new DbTourLogsUpdatedMessage(GetLogs(tour.Id)));
            DeleteTourImage(tour.RouteInformation);
            Log.Information("Tour deleted");
        }

        /// <summary>
        /// Inserts a tour log in DB and sends notification message to all recipients
        /// </summary>
        /// <param name="tourLog">Tour log that should be inserted</param>
        public void CreateTourLog(TourLog tourLog)
        {
            var tpc = new TourPlannerContext();
            tpc.TourLogs.Add(tourLog);
            tpc.SaveChanges();
            Messenger.Send(new DbTourLogsUpdatedMessage(GetLogs(tourLog.TourId)));
            Log.Information("Tour log created");
        }

        /// <summary>
        /// Updates a tour log in DB and sends notification message to all recipients
        /// </summary>
        /// <param name="tourLog">Tour log that should be updated</param>
        public void UpdateTourLog(TourLog tourLog)
        {
            var tpc = new TourPlannerContext();
            tpc.TourLogs.Update(tourLog);
            tpc.SaveChanges();
            Messenger.Send(new DbTourLogsUpdatedMessage(GetLogs(tourLog.TourId)));
            Log.Information("Tour log updated");
        }

        /// <summary>
        /// Deletes a tour log from DB and sends notification message to all recipients
        /// </summary>
        /// <param name="tourLog">Tour log that should be deleted</param>
        public void DeleteTourLog(TourLog tourLog)
        {
            if (tourLog.TourLogImages != null)
            {
                foreach (TourLogImage tli in tourLog.TourLogImages)
                {
                    DeleteTourLogImage(tourLog.TourId, tli);
                }
            }
            var tpc = new TourPlannerContext();
            tpc.TourLogs.Remove(tourLog);
            tpc.SaveChanges();
            Messenger.Send(new DbTourLogsUpdatedMessage(GetLogs(tourLog.TourId)));
            Log.Information("Tour log deleted");
        }

        /// <summary>
        /// Deletes tour image
        /// </summary>
        /// <param name="imagePath">Path of the image that should be deleted</param>
        private void DeleteTourImage(string imagePath)
        {
            try
            {
                File.Delete(imagePath);
                Log.Information("Tour route image deleted");
            }
            catch (Exception e)
            {
                Log.Error("Deleting Tour image failed");
                Debug.WriteLine("Delete Image failed.");
                Debug.WriteLine(e);
            }
        }

        /// <summary>
        /// Gets all specific tour log images
        /// </summary>
        /// <param name="tourLogId"></param>
        /// <returns>List of TourLogImages</returns>
        public List<TourLogImage> GetTourLogImages(int tourLogId)
        {
            Log.Information("Retrieving all specific tour log images");
            var tpc = new TourPlannerContext();
            return new List<TourLogImage>(tpc.TourLogImages.Where(tli => tli.TourLogId == tourLogId));
        }

        /// <summary>
        /// Gets all tour images
        /// </summary>
        public List<TourLogImage> GetAllTourLogImages(int tourId)
        {
            Log.Information("Retrieving all tour images");
            var tpc = new TourPlannerContext();
            return new List<TourLogImage>(tpc.TourLogImages.Where(tli => tli.TourLog.TourId == tourId));
        }

        /// <summary>
        /// Saves given Tour Images
        /// </summary>
        /// <param name="imagePaths">List of image Paths</param>
        public void SaveTourImages(int tourId, int tourLogId, List<TourLogImage> imagePaths)
        {
            var tpc = new TourPlannerContext();
            tpc.TourLogImages.AddRange(imagePaths);
            tpc.SaveChanges();
            Messenger.Send(new TourImagesUpdatedMessage(GetTourLogImages(tourLogId)));
            Messenger.Send(new AllTourImagesUpdatedMessage(GetAllTourLogImages(tourId)));
            Log.Information("Tour images saved");
        }

        /// <summary>
        /// Saves given tour image
        /// </summary>
        public void SaveTourImage(int tourId, int tourLogId, TourLogImage image)
        {
            var tpc = new TourPlannerContext();
            tpc.TourLogImages.Add(image);
            tpc.SaveChanges();
            Messenger.Send(new TourImagesUpdatedMessage(GetTourLogImages(tourLogId)));
            Messenger.Send(new AllTourImagesUpdatedMessage(GetAllTourLogImages(tourId)));
            Log.Information("Tour image saved");
        }

        /// <summary>
        /// Deletes given TourLogImage
        /// </summary>
        public void DeleteTourLogImage(int tourId, TourLogImage image)
        {
            var tpc = new TourPlannerContext();
            tpc.TourLogImages.Remove(image);
            tpc.SaveChanges();
            Messenger.Send(new TourImagesUpdatedMessage(GetTourLogImages(image.TourLogId)));
            Messenger.Send(new AllTourImagesUpdatedMessage(GetAllTourLogImages(tourId)));
            Log.Information("Tour image removed");
        }
    }
}
