﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.JavaScript;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json.Linq;
using Serilog;
using TourPlanner.Models;
using TourPlanner.Models.Enums;

namespace TourPlanner.Services
{
    /// <summary>
    /// Service for handling all MapQuestAPI requests
    /// </summary>
    public static class MapQuestHandler
    {
        private static readonly string BaseUriDistance = ConfigurationManager.AppSettings["MapQuestDistanceUrl"]?.ToString();
        private static readonly string BaseUriStaticMap = ConfigurationManager.AppSettings["MapQuestStaticMapUrl"]?.ToString();

        private static readonly string MapQuestApiKey = ConfigurationManager.AppSettings["MapQuestKey"]?.ToString();

        private static readonly HttpClient DistanceClient = new();

        /// <summary>
        /// MapQuest API Distance Requests
        /// </summary>
        /// <param name="from">Starting point of tour</param>
        /// <param name="to">Destination of tour</param>
        /// <param name="transportType">Type of transportation used</param>
        /// <returns>MapQuestInfos</returns>
        public static async Task<MapQuestInfos> GetMapQuestInfos(string from, string to, TransportType? transportType)
        {
            try
            {
                var builder = new UriBuilder(BaseUriDistance)
                {
                    Port = -1
                };
                var query = HttpUtility.ParseQueryString(builder.Query);
                query["key"] = MapQuestApiKey;
                query["from"] = from;
                query["to"] = to;
                query["unit"] = "k";
                query["routeType"] = transportType.ToString();
                builder.Query = query.ToString() ?? string.Empty;
                string url = builder.ToString();

                string responseBody = await DistanceClient.GetStringAsync(url);
                var content = JObject.Parse(responseBody);

                var info = content.SelectToken("info"); 
                var status = info?.Value<int>("statuscode");

                // TODO: add exception class
                if (status != 0) throw new HttpRequestException("Route not found!");

                var route = content.SelectToken("route");
                var distance = route.Value<double>("distance");
                var time = route.Value<string>("formattedTime");
                var routeMapPath = await GetRouteMap(from, to);

                Log.Information("MapQuestInfos received");
                return new MapQuestInfos(distance, time, routeMapPath);
            }
            catch (HttpRequestException e)
            {
                Log.Warning("Route was not found");
                Debug.WriteLine("\nException Caught!");
                Debug.WriteLine("Message :{0} ", e.Message);
            }

            return null;
        }

        /// <summary>
        /// MapQuest API Static Map Requests
        /// </summary>
        /// <param name="from">Starting point of tour</param>
        /// <param name="to">Destination of tour</param>
        /// <returns>Filepath of route image</returns>
        private static async Task<string> GetRouteMap(string from, string to)
        {
            var builder = new UriBuilder(BaseUriStaticMap)
            {
                Port = -1
            };
            var query = HttpUtility.ParseQueryString(builder.Query);
            query["key"] = MapQuestApiKey;
            query["start"] = from;
            query["end"] = to;
            query["format"] = "png";
            query["size"] = "800,450";
            builder.Query = query.ToString() ?? string.Empty;
            string url = builder.ToString();

            var response = await DistanceClient.GetByteArrayAsync(url);
            Log.Information("Route image received");
            return SaveRouteMap(response);
        }

        /// <summary>
        /// Saves given image in RouteMapImages folder
        /// </summary>
        /// <param name="map">Route image</param>
        /// <returns>Filepath of route image</returns>
        private static string SaveRouteMap(byte[] map)
        {
            string folderPath = Directory.GetParent(Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory()).FullName).FullName).FullName;
            string folderName = Path.Combine(folderPath, "RouteMapImages");
            Directory.CreateDirectory(folderName);
            Log.Information("Image directory created");

            string filename = $"map_{DateTime.Now.ToString("dd'-'MM'-'yyyy'T'HH'_'mm'_'ss")}.png";
            string filePath = Path.Combine(folderName, filename);
            var file = File.Create(filePath);
            file.Write(map);
            file.Close();
            Log.Information("File created");

            return filePath;
        }
    }
}
