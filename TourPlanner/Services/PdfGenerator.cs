﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IronPdf;
using Serilog;
using TourPlanner.Models;
using TourPlanner.Models.Enums;
using TourPlanner.Views;

namespace TourPlanner.Services
{
    public static class PdfGenerator
    {
        private static ChromePdfRenderer renderer = new();
        private static string reportsFolder = ConfigurationManager.AppSettings["ReportsFolder"]?.ToString();

        /// <summary>
        /// Generates a tour-report which contains all information of a single tour and all its associated tour logs
        /// </summary>
        /// <param name="tour">Tour that shall be printed</param>
        public static void GenerateTourReport(Tour tour)
        {
            Directory.CreateDirectory(reportsFolder);
            var tourHandler = new TourHandler();
            var allTourLogs = new List<TourLog>(tourHandler.GetLogs(tour.Id));

            string htmlString = GetTourInformationHtml(tour);
            htmlString += "<hr/>" +
                          "<div style=\"text-align: center\">" +
                          "<h2>Tour Logs</h2>" +
                          "</div>";
            foreach (var tourLog in allTourLogs)
            {
                htmlString += GetTourLogInformationHtml(tourLog);
                htmlString += "<hr/>";
            }

            var pdf = renderer.RenderHtmlAsPdf(htmlString);
            pdf.SaveAs($"{reportsFolder}{tour.Name}_report_{DateTime.Now.ToString("yy-MM-dd")}.pdf");
            Log.Information("Tour report generated");
        }

        /// <summary>
        /// Generates a summarize-report for statistical analysis, which for each tour provides the average time, -distance and rating over all associated tour-logs
        /// Delete later: var list = new List<int>(obsCollection)
        /// </summary>
        /// <param name="tours">List of all tours</param>
        public static void GenerateSummarizeReport(List<Tour> tours)
        {
            Directory.CreateDirectory(reportsFolder);

            string htmlString = "";
            foreach (var tour in tours)
            {
                htmlString += GetTourAverageInformationHtml(tour);
                htmlString += "<hr/>";
            }

            var pdf = renderer.RenderHtmlAsPdf(htmlString);
            pdf.SaveAs($"{reportsFolder}Summarize_report_{DateTime.Now.ToString("yy-MM-dd")}.pdf");
            Log.Information("Summarize report generated");
        }

        private static string GetTourInformationHtml(Tour tour)
        {
            string html = $"<div style=\"text-align: center\">" +
                          $"<h2>{tour.Name}</h2>" +
                          $"<img src=\"{tour.RouteInformation}\" alt=\"Route Image\" width=\"70%\" style=\"border: 1px solid black;\"/>" +
                          $"</div>" +
                          $"<p></p>" +
                          $"<table style=\"width: 100%;\">" +
                          $"<tbody>" +
                          $"<tr style=\"font-style: italic; font-size: 12; color: gray;\">" +
                          $"<td style=\"width: 20%;\">From</td>" +
                          $"<td style=\"width: 20%;\">To</td>" +
                          $"<td style=\"width: 20%;\">Transport Type</td>" +
                          $"<td style=\"width: 20%;\">Distance</td>" +
                          $"<td style=\"width: 20%;\">Estimated Time</td>" +
                          $"</tr>" +
                          $"<tr style=\"font-size: 14;\">" +
                          $"<td>{tour.From}</td>" +
                          $"<td>{tour.To}</td>" +
                          $"<td>{tour.TransportType}</td>" +
                          $"<td>{tour.GetDistance}</td>" +
                          $"<td>{tour.EstimatedTime}</td>" +
                          $"</tr>" +
                          $"</tbody>" +
                          $"</table>";
            return html;
        }

        private static string GetTourLogInformationHtml(TourLog tourLog)
        {
            string htmlBegin = $"<table style=\"width: 100%;\">" +
                          $"<tbody>" +
                          $"<tr style=\"font-style: italic; font-size: 12; color: gray;\">" +
                          $"<td style=\"width: 20%;\">Done at</td>" +
                          $"<td style=\"width: 20%;\">Total Time</td>" +
                          $"<td style=\"width: 20%;\">Difficulty</td>" +
                          $"<td style=\"width: 20%;\">Rating</td>" +
                          $"</tr>" +
                          $"<tr style=\"font-size: 14;\">" +
                          $"<td>{tourLog.GetDate}</td>" +
                          $"<td>{tourLog.TotalTime}</td>" +
                          $"<td>{tourLog.Difficulty}</td>" +
                          $"<td>{tourLog.Rating}</td>" +
                          $"</tr>";

            string htmlComment = $"<tr style=\"font-style: italic; font-size: 12; color: gray;\">" +
                                 $"<td>Comment</td>" +
                                 $"</tr>" +
                                 $"<tr style=\"font-size: 14;\">" +
                                 $"<td colspan=\"4\">{tourLog.Comment}</td>" +
                                 $"</tr>";

            string htmlEnd = $"</tbody>" +
                             $"</table>";

            string html = htmlBegin;
            if (tourLog.Comment != null && tourLog?.Comment.Trim() != "") html += htmlComment;
            html += htmlEnd;
            return html;
        }

        private static string GetTourAverageInformationHtml(Tour tour)
        {
            string html = $"<h3>{tour.Name}</h3>" +
                          $"<table style=\"width: 100%;\">" +
                          $"<tbody>" +
                          $"<tr style=\"font-style: italic; font-size: 12; color: gray;\">" +
                          $"<td style=\"width: 20%;\">Average Time</td>" +
                          $"<td style=\"width: 20%;\">Average Difficulty</td>" +
                          $"<td style=\"width: 20%;\">Popularity</td>" +
                          $"<td style=\"width: 20%;\">Average Rating</td>" +
                          $"</tr>" +
                          $"<tr style=\"font-size: 14;\">" +
                          $"<td>{tour.AverageTime}</td>" +
                          $"<td>{tour.AverageDifficulty}</td>" +
                          $"<td>{tour.Popularity}</td>" +
                          $"<td>{tour.AverageRating}</td>" +
                          $"</tr>" +
                          $"</tbody>" +
                          $"</table>";

            return html;
        }
    }
}
