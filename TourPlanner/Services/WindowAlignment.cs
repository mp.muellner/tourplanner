﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TourPlanner.Views;

namespace TourPlanner.Services
{
    /// <summary>
    /// Simple WindowAlignment class for aligning additional windows to mainWindow
    /// </summary>
    public static class WindowAlignment
    {
        public static double GetLeftPosition(Window window)
        {
            Window mainWindow = Application.Current.MainWindow;
            return mainWindow.Left - window.Width - 10;
        }

        public static double GetTopPosition()
        {
            Window mainWindow = Application.Current.MainWindow;
            return mainWindow.Top;
        }
    }
}
