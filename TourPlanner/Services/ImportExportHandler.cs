﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
using Newtonsoft.Json;
using Serilog;
using TourPlanner.Components;
using TourPlanner.Models;

namespace TourPlanner.Services
{   

    //public interface IImportExportHandler
    //{

    //}
    public static class ImportExportHandler
    {
        private static string exportsFolder = ConfigurationManager.AppSettings["ExportsFolder"];
        public static void ImportTour()
        {
            try
            {
                OpenFileDialog openFileDialog = new();
                string initDirectory = Path.Combine(Directory.GetCurrentDirectory(), exportsFolder);
                openFileDialog.InitialDirectory = System.IO.Path.GetFullPath(initDirectory);
                if (openFileDialog.ShowDialog() == true)
                {
                    string tourJsonString = File.ReadAllText(openFileDialog.FileName);
                    Tour tour = JsonConvert.DeserializeObject<Tour>(tourJsonString);
                    TourHandler tourHandler = new();
                    Tour searchedTour = tourHandler.GetTourById(tour.Id);
                    if (searchedTour == null)
                    {
                        tourHandler.CreateTour(tour);
                    }
                    else
                    {
                        throw new Exception("Tour already exists!");
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                PopUpWindow popUp = new();
                popUp.Text = e.Message;
                popUp.ShowDialog();
            }
        }

        public static void ExportTour(Tour tour)
        {
            Directory.CreateDirectory(exportsFolder);
            Log.Information("Export directory created.");

            TourHandler tourHandler = new();
            List<TourLog> tourLogs = new List<TourLog>(tourHandler.GetLogs(tour.Id));
            tour.TourLogList = tourLogs;

            List<TourLogImage> tourLogImages = new List<TourLogImage>(tourHandler.GetAllTourLogImages(tour.Id));
            tour.TourLogImageList = tourLogImages;

            string tourJsonString = JsonConvert.SerializeObject(tour);

            string filename = $"{tour.Name}_{DateTime.Now.ToString("dd'-'MM'-'yyyy'T'HH'_'mm'_'ss")}.txt";
            string filePath = Path.Combine(exportsFolder, filename);
            File.WriteAllText(filePath, tourJsonString);
            Log.Information("Export file created");
        }
    }
}
