﻿using MahApps.Metro.Controls;
using Serilog;
using System.Configuration;

namespace TourPlanner.Views
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            string loggingPath = ConfigurationManager.AppSettings["TourPlannerLoggingPath"]?.ToString();
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.File(loggingPath, rollingInterval: RollingInterval.Day)
                .CreateLogger();
        }
    }
}
