﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using Serilog;
using TourPlanner.Models.Enums;
using TourPlanner.Models.Messages;
using TourPlanner.Models;
using TourPlanner.Services;

namespace TourPlanner.ViewModels
{
    /// <summary>
    /// ViewModel for EditTourLogWindow
    /// </summary>
    public partial class EditTourLogWindowViewModel : ObservableRecipient
    {
        [ObservableProperty]
        [NotifyCanExecuteChangedFor(nameof(EditTourLogCommand))]
        private DateTime? doneAtInput;

        [ObservableProperty]
        private int hoursInput;

        [ObservableProperty]
        private int minutesInput;

        [ObservableProperty]
        [NotifyCanExecuteChangedFor(nameof(EditTourLogCommand))]
        private Difficulty? difficultyInput;

        [ObservableProperty]
        [NotifyCanExecuteChangedFor(nameof(EditTourLogCommand))]
        private Rating? ratingInput;

        [ObservableProperty]
        private string commentInput;

        [ObservableProperty]
        private TourLog selectedTourLog;

        /// <summary>
        /// Constructor: setting all tour log properties
        /// </summary>
        public EditTourLogWindowViewModel()
        {
            SelectedTourLog = Messenger.Send<SelectedTourLogRequestMessage>();
            DoneAtInput = SelectedTourLog.DoneAt;
            HoursInput = SelectedTourLog.Hours;
            MinutesInput = SelectedTourLog.Minutes;
            DifficultyInput = SelectedTourLog.Difficulty;
            RatingInput = SelectedTourLog.Rating;
            CommentInput = SelectedTourLog.Comment;
            Log.Information("EditTourLogWindow created");
        }

        /// <summary>
        /// Command for updating a tour log
        /// </summary>
        /// <param name="window">Itself for closing the window afterwards</param>
        [RelayCommand(CanExecute = nameof(EditTourLogExecutable))]
        Task EditTourLog(Window window)
        {
            SelectedTourLog.DoneAt = DoneAtInput;
            SelectedTourLog.Hours = HoursInput;
            SelectedTourLog.Minutes = MinutesInput;
            SelectedTourLog.Difficulty = DifficultyInput;
            SelectedTourLog.Rating = RatingInput;
            SelectedTourLog.Comment = CommentInput;

            var tourHandler = new TourHandler();
            tourHandler.UpdateTourLog(SelectedTourLog);
            window?.Close();
            return Task.CompletedTask;
        }

        /// <summary>
        /// Checks if save button should be executable
        /// </summary>
        /// <returns>boolean for determining if the button should be enabled or not</returns>
        private bool EditTourLogExecutable()
        {
            return DoneAtInput != null && DifficultyInput != null && RatingInput != null;
        }
    }
}
