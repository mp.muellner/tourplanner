﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Messaging;
using TourPlanner.Models;
using TourPlanner.Models.Messages;
using TourPlanner.Services;

namespace TourPlanner.ViewModels
{
    public partial class GalleryViewModel : ObservableRecipient
    {
        [ObservableProperty]
        private Tour selectedTour;

        [ObservableProperty]
        private List<TourLogImage> imageList;

        [ObservableProperty]
        private TourLogImage selectedImage;

        [ObservableProperty]
        private string bannerText;

        public GalleryViewModel()
        {
            SelectedTour = Messenger.Send(new SelectedTourRequestMessage());
            SetProperties();
            Messenger.Register<GalleryViewModel, AllTourImagesUpdatedMessage>(this, (r, m) => ImageList = m.ImageList);
            Messenger.Register<GalleryViewModel, SelectedTourMessage>(this, (r, m) => SelectedTour = m.Tour);
        }

        partial void OnSelectedImageChanged(TourLogImage tourLogImage)
        {
            BannerText = tourLogImage != null ? tourLogImage.Name : "Add your pictures by uploading them :)";
        }

        partial void OnSelectedTourChanged(Tour tourLtourogImage)
        {
            SetProperties();
        }

        private void SetProperties()
        {
            if (SelectedTour != null)
            {
                TourHandler tourHandler = new();
                ImageList = tourHandler.GetAllTourLogImages(SelectedTour.Id);
                SelectedImage = ImageList.FirstOrDefault(defaultValue: null);
            }
        }
    }
}
