﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Windows;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using Serilog;
using TourPlanner.Models;
using TourPlanner.Models.Messages;
using TourPlanner.Services;
using TourPlanner.Views;

namespace TourPlanner.ViewModels
{
    /// <summary>
    /// ViewModel for TourListView and its containing views
    /// </summary>
    public partial class TourListViewModel : ObservableRecipient
    {
        [ObservableProperty]
        [NotifyCanExecuteChangedFor(nameof(GenerateSummarizeReportCommand))]
        private ObservableCollection<Tour> allTours;

        [ObservableProperty]
        [NotifyCanExecuteChangedFor(nameof(OpenEditTourWindowCommand))]
        [NotifyCanExecuteChangedFor(nameof(RemoveTourCommand))]
        [NotifyCanExecuteChangedFor(nameof(ExportTourCommand))]
        [NotifyCanExecuteChangedFor(nameof(GenerateTourReportCommand))]
        private Tour selectedTour;

        [ObservableProperty]
        private string searchText;

        /// <summary>
        /// Constructor: Gets all tours, sets selected tour and registers MessageHandling 
        /// </summary>
        public TourListViewModel()
        {
            var tourHandler = new TourHandler();
            AllTours = tourHandler.GetAllTours();
            SelectedTour = AllTours.Count > 0 ? AllTours.First() : null;
            Messenger.Register<TourListViewModel, DbToursUpdatedMessage>(this, (r, m) => AllTours = m.TourCollection);
            Messenger.Register<TourListViewModel, SelectedTourRequestMessage>(this, (r, m) => m.Reply(r.SelectedTour));
            Log.Information("Messenger registered");
        }

        /// <summary>
        /// Command for opening the AddTourWindow
        /// </summary>
        [RelayCommand]
        public static void OpenAddTourWindow()
        {
            var addTourWindow = new AddTourWindow();
            addTourWindow.Top = WindowAlignment.GetTopPosition();
            addTourWindow.Left = WindowAlignment.GetLeftPosition(addTourWindow);
            addTourWindow.ShowDialog();
            Log.Information("Opened AddTourWindow");
        }

        /// <summary>
        /// Command for deleting a tour
        /// </summary>
        [RelayCommand(CanExecute = nameof(TourIsSelected))]
        public void RemoveTour()
        {
            var tourHandler = new TourHandler();
            tourHandler.DeleteTour(SelectedTour);
            SelectedTour = AllTours.Count > 0 ? AllTours.First() : null;
        }

        /// <summary>
        /// Command for opening the EditTourWindow
        /// </summary>
        [RelayCommand(CanExecute = nameof(TourIsSelected))]
        public void OpenEditTourWindow()
        {
            var editTourWindow = new EditTourWindow();
            editTourWindow.Top = WindowAlignment.GetTopPosition();
            editTourWindow.Left = WindowAlignment.GetLeftPosition(editTourWindow);
            editTourWindow.ShowDialog();
            Log.Information("Opened EditTourWindow");
        }

        /// <summary>
        /// Sends a notification message to all its recipients when the selected tour changes
        /// </summary>
        /// <param name="tour">new selected tour</param>
        partial void OnSelectedTourChanged(Tour tour)
        {
            Messenger.Send(new SelectedTourMessage(SelectedTour));
            Log.Information("Selected tour changed");
        }

        /// <summary>
        /// Filters Tour ListView with search bar input
        /// </summary>
        /// <param name="tour">new selected tour</param>
        partial void OnSearchTextChanged(string text)
        {
            ApplyFilter();
        }

        private void ApplyFilter()
        {
            TourHandler tourHandler = new();
            var tempAllTours = tourHandler.GetAllTours();
            var filteredTours = tempAllTours.Where(t =>
                t.Name.Contains(SearchText) ||
                t.Description.Contains(SearchText) ||
                t.From.Contains(SearchText) ||
                t.To.Contains(SearchText) ||
                t.Popularity.ToString().Contains(SearchText) ||
                t.TransportType.ToString().Contains(SearchText)
            );

            var allTourLogs = tourHandler.GetAllLogs();
            var filteredTourLogs = allTourLogs.Where(tl =>
                tl.Comment.Contains(SearchText) ||
                tl.Difficulty.ToString().Contains(SearchText) ||
                tl.Rating.ToString().Contains(SearchText)
            );
            var tourLogIds = filteredTourLogs.Distinct().Select(tl => tl.TourId);

            int count = tempAllTours.Count();
            for (int i = 0; i < count; i++)
            {
                var tour = tempAllTours[i];
                if (!filteredTours.Any(t => t.Id == tour.Id) && !tourLogIds.Contains(tour.Id))
                {
                    tempAllTours.Remove(tour);
                }
            }

            AllTours = tempAllTours;
            SelectedTour = AllTours.Count() > 0 ? AllTours.First() : null;
        }

        /// <summary>
        /// Checks if a tour is selected
        /// </summary>
        /// <returns>boolean</returns>
        private bool TourIsSelected()
        {
            if (SelectedTour != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks if tours exist
        /// </summary>
        /// <returns>boolean</returns>
        private bool ToursExist()
        {
            if (AllTours.Count > 0)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Command for printing a tour report
        /// </summary>
        [RelayCommand(CanExecute = nameof(TourIsSelected))]
        public void GenerateTourReport()
        {
            PdfGenerator.GenerateTourReport(SelectedTour);
        }

        /// <summary>
        /// Command for printing a summarize report
        /// </summary>
        [RelayCommand(CanExecute = nameof(ToursExist))]
        public void GenerateSummarizeReport()
        {
            var tourList = new List<Tour>(AllTours);
            PdfGenerator.GenerateSummarizeReport(tourList);
        }

        /// <summary>
        /// Command for closing the application
        /// </summary>
        [RelayCommand]
        public void CloseApplication()
        {
            Application.Current.MainWindow.Close();
        }

        /// <summary>
        /// Command for exporting a tour
        /// </summary>
        [RelayCommand(CanExecute = nameof(TourIsSelected))]
        public void ExportTour()
        {
            ImportExportHandler.ExportTour(SelectedTour);
        }

        /// <summary>
        /// Command for exporting a tour
        /// </summary>
        [RelayCommand]
        public void ImportTour()
        {
            ImportExportHandler.ImportTour();
        }

    }
}
