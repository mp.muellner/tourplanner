﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using ControlzEx.Standard;
using MahApps.Metro.Actions;
using Serilog;
using TourPlanner.Models;
using TourPlanner.Models.Enums;
using TourPlanner.Services;

namespace TourPlanner.ViewModels
{
    /// <summary>
    /// ViewModel for AddTourWindow
    /// </summary>
    public partial class AddTourWindowViewModel : ObservableRecipient
    {
        [ObservableProperty]
        [NotifyCanExecuteChangedFor(nameof(CreateNewTourCommand))]
        private string _nameInput = "";

        [ObservableProperty]
        private string _descriptionInput = "";

        [ObservableProperty]
        [NotifyCanExecuteChangedFor(nameof(CreateNewTourCommand))]
        private string _fromInput = "";

        [ObservableProperty]
        [NotifyCanExecuteChangedFor(nameof(CreateNewTourCommand))]
        private string _toInput = "";

        [ObservableProperty] [NotifyCanExecuteChangedFor(nameof(CreateNewTourCommand))]
        private TransportType? _transportTypeInput = null;

        [ObservableProperty]
        private string isDisclaimerShown = "Hidden";

        /// <summary>
        /// Command for creating a new tour
        /// </summary>
        /// <param name="window">Itself for closing the window afterwards</param>
        [RelayCommand(CanExecute = nameof(CreateNewTourExecutable))]
        async Task CreateNewTour(Window window)
        {

            MapQuestInfos mapQuestInfo = await MapQuestHandler.GetMapQuestInfos(FromInput, ToInput, TransportTypeInput);
            if (mapQuestInfo == null)
            {
                DisplayDisclaimer();
            }
            else
            {
                var newTour = new Tour()
                {
                    Name = NameInput,
                    Description = DescriptionInput,
                    From = FromInput,
                    To = ToInput,
                    TransportType = TransportTypeInput,
                    EstimatedTime = mapQuestInfo.EstimatedTime,
                    RouteInformation = mapQuestInfo.RoutePath,
                    TourDistance = mapQuestInfo.Distance
                };
                var tourHandler = new TourHandler();
                tourHandler.CreateTour(newTour);
                IsDisclaimerShown = "Hidden";
                if (window != null) window.Close();
            }
        }

        /// <summary>
        /// Displays a disclaimer for 3 seconds
        /// </summary>
        async Task DisplayDisclaimer()
        {
            IsDisclaimerShown = "Visible";
            Log.Information("Disclaimer is visible");
            await Task.Delay(3_000);
            IsDisclaimerShown = "Hidden";
            Log.Information("Disclaimer is hidden");
        }

        /// <summary>
        /// Checks if add new tour button should be executable
        /// </summary>
        /// <returns>boolean for determining if the button should be enabled or not</returns>
        private bool CreateNewTourExecutable()
        {
            if (NameInput != "" && FromInput != "" && ToInput != "" && TransportTypeInput != null)
            {
                return true;
            }

            return false;
        }

    }
}
