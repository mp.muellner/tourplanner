﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using Serilog;
using TourPlanner.Models;
using TourPlanner.Models.Enums;
using TourPlanner.Models.Messages;
using TourPlanner.Services;

namespace TourPlanner.ViewModels
{
    /// <summary>
    /// ViewModel for AddTourLogWindow
    /// </summary>
    public partial class AddTourLogWindowViewModel : ObservableRecipient
    {

        [ObservableProperty]
        [NotifyCanExecuteChangedFor(nameof(CreateNewTourLogCommand))]
        private DateTime? doneAtInput;

        [ObservableProperty]
        private int hoursInput;

        [ObservableProperty]
        private int minutesInput;

        [ObservableProperty]
        [NotifyCanExecuteChangedFor(nameof(CreateNewTourLogCommand))]
        private Difficulty? difficultyInput;

        [ObservableProperty]
        [NotifyCanExecuteChangedFor(nameof(CreateNewTourLogCommand))]
        private Rating? ratingInput;

        [ObservableProperty]
        private string commentInput;

        [ObservableProperty]
        private Tour selectedTour;

        /// <summary>
        /// Constructor: Sets the selected Tour by sending a request to ListViewModel
        /// + sets pre-selected values
        /// </summary>
        public AddTourLogWindowViewModel()
        {
            SelectedTour = Messenger.Send<SelectedTourRequestMessage>();
            doneAtInput = DateTime.Now;
            Log.Information("AddTourLogWindow created");
        }

        /// <summary>
        /// Command for creating a new tour log 
        /// </summary>
        /// <param name="window">Itself for closing the window</param>
        [RelayCommand(CanExecute = nameof(CreateNewTourLogExecutable))]
        Task CreateNewTourLog(Window window)
        {
            TourLog tourLog = new TourLog()
            {
                DoneAt = DoneAtInput,
                Comment = CommentInput,
                Difficulty = DifficultyInput,
                Hours = HoursInput,
                Minutes = MinutesInput,
                Rating = RatingInput,
                TourId = SelectedTour.Id
            };
            var tourHandler = new TourHandler();
            tourHandler.CreateTourLog(tourLog);
            window?.Close();
            return Task.CompletedTask;
        }

        /// <summary>
        /// Checks if add new tour log button should be executable
        /// </summary>
        /// <returns>boolean for determining if the button should be enabled or not</returns>
        private bool CreateNewTourLogExecutable()
        {
            return DoneAtInput != null && DifficultyInput != null && RatingInput != null;
        }
    }
}
