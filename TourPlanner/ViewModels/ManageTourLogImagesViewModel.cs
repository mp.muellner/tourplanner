﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using Microsoft.Win32;
using Newtonsoft.Json;
using Serilog;
using TourPlanner.Models;
using TourPlanner.Models.Messages;
using TourPlanner.Services;

namespace TourPlanner.ViewModels
{
    public partial class ManageTourLogImagesViewModel : ObservableRecipient
    {
        [ObservableProperty]
        private TourLog selectedTourLog;

        [ObservableProperty]
        private List<TourLogImage> imageList;

        [ObservableProperty]
        private TourLogImage selectedImage;

        [ObservableProperty]
        private string bannerText;

        public ManageTourLogImagesViewModel()
        {
            TourHandler tourHandler = new();
            SelectedTourLog = Messenger.Send<SelectedTourLogRequestMessage>();
            ImageList = tourHandler.GetTourLogImages(SelectedTourLog.Id);
            SelectedImage = ImageList.FirstOrDefault(defaultValue: null);
            Messenger.Register<ManageTourLogImagesViewModel, TourImagesUpdatedMessage>(this, (r, m) => ImageList = m.ImageList);
        }

        partial void OnSelectedImageChanged(TourLogImage tourLogImage)
        {
            BannerText = tourLogImage != null ? tourLogImage.Name : "Add your pictures by uploading them :)";
        }

        [RelayCommand]
        public void UploadImages()
        {
            var openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = true;
            if (openFileDialog.ShowDialog() == true)
            {
                TourHandler tourHandler = new TourHandler();
                string[] allFileNames = openFileDialog.FileNames;
                List<TourLogImage> images = new List<TourLogImage>();
                foreach (string fileName in allFileNames)
                {
                    TourLogImage image = new();
                    image.TourLogId = SelectedTourLog.Id;
                    image.Name = System.IO.Path.GetFileName(fileName);
                    image.ImagePath = fileName;
                    images.Add(image);
                }
                tourHandler.SaveTourImages(SelectedTourLog.TourId, SelectedTourLog.Id, images);
            }
        }

        [RelayCommand]
        public void RemoveImage()
        {
            TourHandler tourHandler = new();
            tourHandler.DeleteTourLogImage(SelectedTourLog.TourId, SelectedImage);
        }

    }
}
