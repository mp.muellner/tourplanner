﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using TourPlanner.Models;
using TourPlanner.Services;
using TourPlanner.Models.Enums;
using TourPlanner.Models.Messages;
using TransportType = TourPlanner.Models.Enums.TransportType;
using Serilog;

namespace TourPlanner.ViewModels
{
    public partial class EditTourWindowViewModel : ObservableRecipient
    {
        [ObservableProperty]
        private Tour selectedTour;

        [ObservableProperty]
        [NotifyCanExecuteChangedFor(nameof(EditTourCommand))]
        private string nameInput;

        [ObservableProperty]
        private string descriptionInput;

        [ObservableProperty]
        [NotifyCanExecuteChangedFor(nameof(EditTourCommand))]
        private string fromInput;

        [ObservableProperty]
        [NotifyCanExecuteChangedFor(nameof(EditTourCommand))]
        private string toInput;

        [ObservableProperty]
        [NotifyCanExecuteChangedFor(nameof(EditTourCommand))]
        private TransportType? transportTypeInput;

        [ObservableProperty]
        private string isDisclaimerShown = "Hidden";

        /// <summary>
        /// Constructor: Gets selected tour by sending a request to TourListViewModel
        /// + sets all tour properties
        /// </summary>
        public EditTourWindowViewModel()
        {
            SelectedTour = Messenger.Send<SelectedTourRequestMessage>();
            NameInput = SelectedTour.Name;
            DescriptionInput = SelectedTour.Description;
            FromInput = SelectedTour.From;
            ToInput = SelectedTour.To;
            TransportTypeInput = SelectedTour.TransportType;
            Log.Information("EditTourWindow created");
        }

        /// <summary>
        /// Command for updating a tour
        /// </summary>
        /// <param name="window">Itself to close window afterwards</param>
        /// <returns></returns>
        [RelayCommand(CanExecute = nameof(EditTourExecutable))]
        async Task EditTour(Window window)
        {
            var tourHandler = new TourHandler();
            if (SelectedTour.From != FromInput || SelectedTour.To != ToInput || SelectedTour.TransportType != TransportTypeInput)
            {
                MapQuestInfos mapQuestInfo = await MapQuestHandler.GetMapQuestInfos(FromInput, ToInput, TransportTypeInput);
                if (mapQuestInfo == null)
                {
                    DisplayDisclaimer();
                }
                else
                {
                    SelectedTour.Name = NameInput;
                    SelectedTour.Description = DescriptionInput;
                    SelectedTour.From = FromInput;
                    SelectedTour.To = ToInput;
                    SelectedTour.TransportType = TransportTypeInput;
                    SelectedTour.EstimatedTime = mapQuestInfo.EstimatedTime;
                    SelectedTour.TourDistance = mapQuestInfo.Distance;
                    SelectedTour.RouteInformation = mapQuestInfo.RoutePath;

                    tourHandler.UpdateTour(SelectedTour);
                    if (window != null) window.Close();
                    IsDisclaimerShown = "Hidden";
                }
            }
            else
            {
                SelectedTour.Name = NameInput;
                SelectedTour.Description = DescriptionInput;
                tourHandler.UpdateTour(SelectedTour);
                if (window != null) window.Close();
                IsDisclaimerShown = "Hidden";
            }
        }

        /// <summary>
        /// Displays a disclaimer for 3 seconds
        /// </summary>
        /// <returns></returns>
        async Task DisplayDisclaimer()
        {
            IsDisclaimerShown = "Visible";
            Log.Information("Disclaimer is visible");
            await Task.Delay(3_000);
            IsDisclaimerShown = "Hidden";
            Log.Information("Disclaimer is hidden");
        }

        /// <summary>
        /// Checks if save button should be executable
        /// </summary>
        /// <returns>boolean for determining if the button should be enabled or not</returns>
        private bool EditTourExecutable()
        {
            if (NameInput != "" && FromInput != "" && ToInput != "" && TransportTypeInput != null)
            {
                return true;
            }

            return false;
        }

    }
}
