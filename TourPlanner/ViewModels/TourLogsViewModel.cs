﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using TourPlanner.Models;
using TourPlanner.Services;
using TourPlanner.Models.Enums;
using TransportType = TourPlanner.Models.Enums.TransportType;
using System.Collections.ObjectModel;
using System.Net.Mime;
using System.Windows;
using CommunityToolkit.Mvvm.Messaging;
using Serilog;
using TourPlanner.Models.Messages;
using TourPlanner.Views;

namespace TourPlanner.ViewModels
{
    /// <summary>
    /// ViewModel for TourLogsView
    /// </summary>
    public partial class TourLogsViewModel : ObservableRecipient
    {
        [ObservableProperty] 
        private ObservableCollection<TourLog> allTourLogs;

        [ObservableProperty]
        [NotifyCanExecuteChangedFor(nameof(OpenEditTourLogWindowCommand))]
        [NotifyCanExecuteChangedFor(nameof(RemoveTourLogCommand))]
        [NotifyCanExecuteChangedFor(nameof(OpenTourLogImageManagerCommand))]
        private TourLog selectedTourLog;

        [ObservableProperty] 
        [NotifyCanExecuteChangedFor(nameof(OpenAddTourLogWindowCommand))]
        public Tour selectedTour;

        /// <summary>
        /// Constructor: Registers to Messages and requests selected tour
        /// </summary>
        public TourLogsViewModel()
        {
            Messenger.Register<TourLogsViewModel, DbTourLogsUpdatedMessage>(this, (r, m) => AllTourLogs = m.TourLogsCollection);
            Messenger.Register<TourLogsViewModel, SelectedTourMessage>(this, (r, m) => r.SelectedTour = m.Tour);
            Messenger.Register<TourLogsViewModel, SelectedTourLogRequestMessage>(this, (r, m) => m.Reply(r.SelectedTourLog));
            SelectedTour = Messenger.Send<SelectedTourRequestMessage>();
            Log.Information("Messenger registered");
        }

        /// <summary>
        /// Retrieves all logs when the selected tour changes
        /// </summary>
        /// <param name="tour">changed selected tour</param>
        partial void OnSelectedTourChanged(Tour tour)
        {
            if (SelectedTour != null)
            {
                var tourHandler = new TourHandler();
                AllTourLogs = tourHandler.GetLogs(SelectedTour.Id);
            }
            else
            {
                AllTourLogs.Clear();
            }
        }

        /// <summary>
        /// Command for opening the AddTourLogWindow
        /// </summary>
        [RelayCommand(CanExecute = nameof(TourIsSelected))]
        public void OpenAddTourLogWindow()
        {
            var addTourLogWindow = new AddTourLogWindow();
            addTourLogWindow.Top = WindowAlignment.GetTopPosition();
            addTourLogWindow.Left = WindowAlignment.GetLeftPosition(addTourLogWindow);
            addTourLogWindow.ShowDialog();
            Log.Information("Opened AddTourLogWindow");
        }

        /// <summary>
        /// Command for opening the EditTourLogWindow
        /// </summary>
        [RelayCommand(CanExecute = nameof(TourLogIsSelected))]
        public void OpenEditTourLogWindow()
        {
            var editTourLogWindow = new EditTourLogWindow();
            editTourLogWindow.Top = WindowAlignment.GetTopPosition();
            editTourLogWindow.Left = WindowAlignment.GetLeftPosition(editTourLogWindow);
            editTourLogWindow.ShowDialog();
            Log.Information("Opened EditTourLogWindow");
        }

        /// <summary>
        /// Command for deleting selected tour log
        /// </summary>
        [RelayCommand(CanExecute = nameof(TourLogIsSelected))]
        public void RemoveTourLog()
        {
            var tourHandler = new TourHandler();
            tourHandler.DeleteTourLog(SelectedTourLog);
        }

        /// <summary>
        /// Checks if a tour log is selected
        /// </summary>
        /// <returns>boolean</returns>
        private bool TourLogIsSelected()
        {
            if (SelectedTourLog != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks if a tour is selected
        /// </summary>
        /// <returns>boolean</returns>
        private bool TourIsSelected()
        {
            if (SelectedTour != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Command for opening the tour log manager
        /// </summary>
        [RelayCommand(CanExecute = nameof(TourLogIsSelected))]
        public void OpenTourLogImageManager()
        {
            var tourLogImageManager = new ManageTourLogImagesView();
            tourLogImageManager.Top = WindowAlignment.GetTopPosition();
            tourLogImageManager.Left = WindowAlignment.GetLeftPosition(tourLogImageManager);
            tourLogImageManager.ShowDialog();
        }

    }
}
