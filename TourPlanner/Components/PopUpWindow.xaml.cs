﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;

namespace TourPlanner.Components
{
    /// <summary>
    /// Interaktionslogik für PopUpWindow.xaml
    /// </summary>
    public partial class PopUpWindow : MetroWindow
    {
        /// <summary>
        /// Property for setting Text Content
        /// </summary>
        public string Text { get; set; }

        public PopUpWindow()
        {
            InitializeComponent();
            DataContext = this;
            Window mainWindow = Application.Current.MainWindow;
            Left = mainWindow.Left + mainWindow.Width / 2 - Width / 2;
            Top = mainWindow.Top + mainWindow.Height / 2 - Height / 2;
        }
    }
}
