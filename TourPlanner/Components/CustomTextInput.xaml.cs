﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TourPlanner.Components
{
    /// <summary>
    /// Interaktionslogik für CustomTextInput.xaml
    /// </summary>
    public partial class CustomTextInput : UserControl
    {
        /// <summary>
        /// Constructor: Initializes Component and set DataContext of LayoutGrid
        /// </summary>
        public CustomTextInput()
        {
            InitializeComponent();
            LayoutGrid.DataContext = this;
        }

        /// <summary>
        /// DependencyProperty of Text Input Value
        /// </summary>
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register(nameof(CustomTextInput.Value),
                typeof(string),
                typeof(CustomTextInput),
                new FrameworkPropertyMetadata(default(string), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        /// <summary>
        /// Property for Label Content
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Property for TextBlock Text
        /// </summary>
        public string Value
        {
            get => (string)GetValue(ValueProperty);
            set => SetValue(ValueProperty, value);
        }

        /// <summary>
        /// Property for displayed Watermark inside TextBlock
        /// </summary>
        public string Watermark { get; set; }
    }
}
