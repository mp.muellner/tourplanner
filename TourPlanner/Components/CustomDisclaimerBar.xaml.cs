﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TourPlanner.Components
{
    /// <summary>
    /// Interaktionslogik für CustomDisclaimerBar.xaml
    /// </summary>
    public partial class CustomDisclaimerBar : UserControl
    {
        /// <summary>
        /// Constructor: Initializes User Control and sets the DataContext of the LayoutGrid
        /// </summary>
        public CustomDisclaimerBar()
        {
            InitializeComponent();
            LayoutGrid.DataContext = this;
        }

        /// <summary>
        /// DependencyProperty for BackgroundColor
        /// </summary>
        public static readonly DependencyProperty BgColorProperty =
            DependencyProperty.Register(nameof(CustomDisclaimerBar.BgColor),
                typeof(string),
                typeof(CustomDisclaimerBar),
                new FrameworkPropertyMetadata(default(string), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        /// <summary>
        /// DependencyProperty for Visibility of Disclaimer
        /// </summary>
        public static readonly DependencyProperty IsShownProperty =
            DependencyProperty.Register(nameof(CustomDisclaimerBar.IsShown),
                typeof(string),
                typeof(CustomDisclaimerBar),
                new FrameworkPropertyMetadata(default(string), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        /// <summary>
        /// Property for used BackgroundColor
        /// </summary>
        public string BgColor
        {
            get => (string)GetValue(BgColorProperty);
            set => SetValue(BgColorProperty, value);
        }

        /// <summary>
        /// Property for displayed Text
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Property for Visibility of Disclaimer
        /// </summary>
        public string IsShown
        {
            get => (string)GetValue(IsShownProperty);
            set => SetValue(IsShownProperty, value);
        }
    }
}
