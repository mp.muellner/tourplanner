﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.AvalonDock.Layout;

namespace TourPlanner.Components
{
    /// <summary>
    /// Interaktionslogik für DisplayValueWithLabel.xaml
    /// </summary>
    public partial class DisplayValueWithLabel : UserControl
    {
        /// <summary>
        /// DependencyProperty of TextBlock Text
        /// </summary>
        public static readonly DependencyProperty ValueProperty = 
            DependencyProperty.Register(nameof(DisplayValueWithLabel.Value), 
                typeof(string), 
                typeof(DisplayValueWithLabel), 
                new FrameworkPropertyMetadata(default(string), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        /// <summary>
        /// Constructor: Initializes Component and sets DataContext of LayoutGrid
        /// </summary>
        public DisplayValueWithLabel()
        {
            InitializeComponent();
            LayoutGrid.DataContext = this;
        }

        /// <summary>
        /// Property for setting Label Content
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Property for setting TextBlock Text
        /// </summary>
        public string Value
        {
            get => (string)GetValue(ValueProperty);
            set => SetValue(ValueProperty, value);
        }
    }
}
